/* ************************************************************************** */
/** Descriptive File Name
 
 @Company
 Company Name
 
 @File Name
 filename.c
 
 @Summary
 Brief description of the file.
 
 @Description
 Describe the purpose of this file.
 */
/* ************************************************************************** */
#include "../Inc/main.h"




void main (void){
    

    initialize_settings ();
    
    while (1){
        __delay_ms(100);
        LATAbits.LATA5 ^= 1;
    }
}


/**
 * @brief    Initializes ports (inputs and outputs), analog (Potentiometer)
 * @param    <void>
 * @retval   <void>
 */
void initialize_settings () {
    OSCCONbits.IRCF = 0b1111;    
    //Set LED's as Output 
    TRISCbits.TRISC5 = 0;
    TRISAbits.TRISA2 = 0;
    TRISAbits.TRISA5 = 0;
    TRISAbits.TRISA1 = 0;

    //UART TX (RB7) as Output
    TRISBbits.TRISB7 = 0;

    //UART RX (RB5) as Input
    TRISBbits.TRISB5 = 1;

    //Set SW1 as input
    TRISCbits.TRISC4 = 1;

    //Set Potentiometer as input
    TRISCbits.TRISC0 = 1;
    
    //Set Potentiometer as analog input
    ANSELCbits.ANSC0 = 1;
    
    //Turn off leds
    PORTCbits.RC5 = 0;
    PORTAbits.RA1 = 0;
    PORTAbits.RA2 = 0;
    PORTAbits.RA5 = 0;
}



/* *****************************************************************************
 End of File
 */
