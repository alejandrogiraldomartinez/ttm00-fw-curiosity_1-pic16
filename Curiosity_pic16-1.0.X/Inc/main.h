#include <xc.h>
#include <pic16f1619.h>
//#include <stdint.h>

#define _XTAL_FREQ 16000000

// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection Bits (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config PWRTE = OFF      // Power-up Timer Enable (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = ON        // Internal/External Switch Over (Internal External Switch Over mode is enabled)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is enabled)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config PPS1WAY = ON     // Peripheral Pin Select one-way control (The PPSLOCK bit cannot be cleared once it is set by software)
#pragma config ZCD = OFF        // Zero Cross Detect Disable Bit (ZCD disable.  ZCD can be enabled by setting the ZCDSEN bit of ZCDCON)
#pragma config PLLEN = OFF      // PLL Enable Bit (4x PLL is enabled when software sets the SPLLEN bit)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LPBOR = OFF      // Low-Power Brown Out Reset (Low-Power BOR is disabled)
#pragma config LVP = ON         // Low-Voltage Programming Enable (Low-voltage programming enabled)

// CONFIG3
#pragma config WDTCPS = WDTCPS1F// WDT Period Select (Software Control (WDTPS))
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config WDTCWS = WDTCWSSW// WDT Window Select (Software WDT window size control (WDTWS bits))
#pragma config WDTCCS = SWC     // WDT Input Clock Selector (Software control, controlled by WDTCS bits)

// #pragma config statements should precede project file includes.

typedef unsigned char u8;   
typedef unsigned short u16;      
typedef unsigned int u32;    
typedef unsigned long long u64;

typedef  char i8;   
typedef  short i16;      
typedef  int i32;    
typedef  long long i64;

typedef const unsigned char cu8;   
typedef const unsigned short cu16;      
typedef const unsigned int cu32;    
typedef const unsigned long long cu64;

typedef const char ci8;   
typedef const short ci16;      
typedef const int ci32;    
typedef const long long ci64;

typedef volatile unsigned char vu8;   
typedef volatile unsigned short vu16;      
typedef volatile unsigned int vu32;    
typedef volatile unsigned long long vu64;

typedef volatile  char vi8;   
typedef volatile  short vi16;      
typedef volatile  int vi32;    
typedef volatile  long long vi64;

typedef unsigned char uint8_t;  
typedef unsigned short uint16_t;      
typedef unsigned int uint32_t;    
typedef unsigned long long uint64_t;

typedef char int8_t;    
typedef short int16_t;      
typedef int int32_t;    
typedef long long int64_t;

typedef const unsigned char cuint8_t;    
typedef const unsigned short cuint16_t;      
typedef const unsigned int cuint32_t;    
typedef const unsigned long long cuint64_t;

typedef const char cint8_t;    
typedef const short cint16_t;      
typedef const int cint32_t;    
typedef const long long cint64_t;

typedef volatile unsigned char vuint8_t;    
typedef volatile unsigned short vuint16_t;      
typedef volatile unsigned int vuint32_t;    
typedef volatile unsigned long long vuint64_t;

typedef volatile char vint8_t;    
typedef volatile short vint16_t;      
typedef volatile int vint32_t;    
typedef volatile long long vint64_t;





void initialize_settings ();